# Welcome to the Chatvizard  

---  
This project is a chat visualization tool that can be used to compare writing habits!  
You can have a look at viz.ipynb for an example! 

---

If you want to visualize your own chat history, using this script won't expose your data to me or anyone.   
This tool processes all data locally on your machine, it does not collect any data nor does it use an internet connection. If you are worried about your chat being exposed, you can even turn off your connection before running this. You can also take a look at the source code to be sure.

---  

## Why use chatvizard when there are so many existing chat visualisers?
In short: **Customisation & Privacy!**  
- this tool can be used to get statistics on your personal quirks - want to find out who writes your insider meme more often? Who swears more often? Choose your own phrase or regular expression and dig in!
- this tool uses NLP techniques, whereas others only use statistics
    - this allows insight filtering the boring statistics by generalizing across the colorful morphology of written language

That said, some people out there did some cool work, I can recommend [this analyzer](https://github.com/mowolf/ChatAnalyzer)!

---

## How to use this tool?
You have to export your chat data, download this script and point the script to the exported data! I am using Python3.8 and Jupyter Notebooks, so you will need to have those installed, too. 


### Exporting Telegram Chats
To find out how to export your chat, [click here](https://telegram.org/blog/export-and-more).  
Make sure to only download text messages and unclick other kinds of media!


### Preparing and running the script
To prepare the script, follow the following steps in your terminal:
- download it by running `git clone https://gitlab.com/DavidLeonardWenzel/chatvizard`
- install the required python modules by running `python -m pip install -r requirements.txt` (if you used pip before, consider using a virutal env or using your systemwide package manager instead!)
- install the language information by running `python -m spacy download en_core_web_sm --user`
- open `viz.ipynb`, in Jupyter Notebook (you need to have installed jupyter running a python 3.8 kernel!)
- you will see a "Path" variable! Point to your unzipped ChatExport folder here!
- if you want your own memes to be counted and visualized, have a look at `resources/phrases.toml`. You can add your own regular expression here and it will be included in the analysis!
- run all and enjoy!