import src.Sender as Sender
import pickle
from datetime import datetime, timedelta
import src.parser as parser
import operator
import os.path


def prep(path):
    path = check_path(path)
    if not os.path.isfile(path+"_data.dat"):
        chat = parser.parse(parser.soupify_raw_data(
            parser.get_raw_data(path+"/")))
        parser.save(chat, path)
        print(f"parsed and dumped {len(chat)} messages!")
    chat = load(path)
    senders = analyze(chat)
    A, B = list(senders.values())[0:]
    return A, B


def check_path(path):
    last_letter = path[-1]
    if last_letter == "/":
        return path[:-1]
    else:
        return path


def identify_senders(chat, search_size=100):
    """looks through messages up to search size and creates sender classes"""
    known_senders = set()
    if len(chat) < search_size:
        search_size = len(chat)
    for msg in chat[:100]:
        known_senders.add(msg.sender)
    list_of_senders = {}
    for name in known_senders:
        list_of_senders[name] = Sender.Sender(name, chat)
    print(f"found the senders {known_senders}")
    assert(len(list_of_senders) == 2)
    return list_of_senders


def load(path):
    print("loading..")
    return pickle.load(open(path+"_data.dat", "rb"))


def pprint(msg):
    print(f"{msg.sender}, {msg.time}:\t{msg.content}")


def get_response_treshhold():
    t = datetime.strptime("06:00:00", "%H:%M:%S")
    return timedelta(hours=t.hour, minutes=t.minute, seconds=t.second)


def get_chat_log():
    return {"lastSender": None,
            "lastSendTime": None,
            "lastContent": None,
            "chain": 1,
            "response_threshhold": get_response_treshhold()}


def analyze(chat):
    senders = identify_senders(chat)
    chat_log = get_chat_log()
    for i, msg in enumerate(chat):
        senders[msg.sender].log(msg)
        log_conversation_info(senders, msg, chat_log, i)
    for sender in senders:
        senders[sender].calc_averages()
        senders[sender].finalize()
    print("Done analyzing!")
    return senders


def log_conversation_info(senders, msg, chat_log, i):
    if i != 0:
        if msg.sender == chat_log["lastSender"]:
            # same person as last time wrote:
            if msg.content != "media":
                chat_log["chain"] += 1
        else:
            # different person as last time wrote, change of speaker occured:
            response_time = msg.time - chat_log["lastSendTime"]
            if response_time < chat_log["response_threshhold"]:
                # log response time for the sender (which is the current sender)
                # overall:
                senders[msg.sender].response_times.append(response_time)
                # per weekday:
                senders[msg.sender].weekday_response_times[msg.time.weekday()
                                                           ].append(response_time)
                # per day:
                # senders[msg.sender].date_response_times[msg.time.date()
                #                                        ].append(response_time)
                # log how long the chain of uninterrupted messages was for the last sender (and reset chain):
                senders[chat_log["lastSender"]].log_chain_content(chat_log)

    chat_log["lastSender"] = msg.sender
    chat_log["lastSendTime"] = msg.time
    chat_log["lastContent"] = msg.content


def sort_dict_by_value(x):
    # dependency: import operator
    sorted_x = sorted(x.items(), key=operator.itemgetter(1))
    sorted_x.reverse()  # remove to go from small to big
    return sorted_x


def daterange(date1, date2):
    for n in range(int((date2 - date1).days)+1):
        yield date1 + timedelta(n)


def get_weeks_between_dates(date1, date2):
    return abs(date1-date2).days // 7


def get_days_between_dates(date1, date2):
    return abs(date1-date2).days


def get_day_range(start_date, end_date):
    days = {}
    for dt in daterange(start_date, end_date):
        days[dt.date()] = 0
    return days
